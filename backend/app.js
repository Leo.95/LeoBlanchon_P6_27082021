//MIDDLEWARE V2 GET USE//
app.use("/api/stuff", (req, res, next) => {
    const stuff = [{
            _id: "oeihfzeoi",
            title: "Mon premier objet",
            description: "Les infos de mon premier objet",
            imageUrl: "https://cdn.pixabay.com/photo/2019/06/11/18/56/camera-4267692_1280.jpg",
            price: 4900,
            userId: "qsomihvqios",
        },
        {
            _id: "oeihfzeomoihi",
            title: "Mon deuxième objet",
            description: "Les infos de mon deuxième objet",
            imageUrl: "https://cdn.pixabay.com/photo/2019/06/11/18/56/camera-4267692_1280.jpg",
            price: 2900,
            userId: "qsomihvqios",
        },
    ];
    res.status(200).json(stuff);
});
//FIN
//MIDDLEWARE HEADER//
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, PATCH, OPTIONS"
    );
    next();
});
//FIN//
//IMPORT BODYPARSER//
const bodyParser = require("body-parser");
//FIN//
//FONCTION EXTRAIRE JSON BODYPARSER//
app.use(bodyParser.json());
//FIN//
//ROUTE POST BODYPARSER//
app.post("/api/stuff", (req, res, next) => {
    console.log(req.body);
    res.status(201).json({
        message: "Objet créé !",
    });
});
//FIN//
//IMPORT MONGOOSE//
const mongoose = require("mongoose");
//END//
//CONNEXION CLUSTER MONGODB//
const { MongoClient } = require('mongodb');
const uri = "mongodb+srv://leo_user_2021:<Pepita92320@>@cluster0.va9ea.mongodb.net/Piiquante?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect(err => {
    const collection = client.db("test").collection("devices");
    // perform actions on the collection object
    client.close();
});
.then(() => console.log("Connexion à MongoDB réussie !"))
    .catch(() => console.log("Connexion à MongoDB échouée !"));
//FIN//