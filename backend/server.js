/*PACKAGE HTTP SERVEUR NODE//
const http = require("http");

const server = http.createServer((req, res) => {
  res.end("Voilà la réponse du serveur !");
});

server.listen(process.env.PORT || 3000);
//FIN*/

//APP EXPRESS SUR SERVEUR NODE//
const http = require("http");
const app = require("./app");

app.set("port", process.env.PORT || 3000);
const server = http.createServer(app);

server.listen(process.env.PORT || 3000);
//FIN//
//REPONSE FONCTIONNEMENT EXPRESS//
const express = require("express");

const app = express();

app.use((req, res) => {
    res.json({ message: "Votre requête a bien été reçue !" });
});

module.exports = app;
//FIN//